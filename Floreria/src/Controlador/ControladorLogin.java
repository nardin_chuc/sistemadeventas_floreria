/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Main.Conexion;
import Vistas.Login;
import Vistas.PanelAdmin;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author CCNAR
 */
public class ControladorLogin implements ActionListener, KeyListener, MouseListener{

    Login lg;
    PanelAdmin pna;
    String ROL;
    String nombreAux;

    public ControladorLogin(Login lg) {
        this.lg = lg;
        this.lg.btn_ingresar.addActionListener(this);
        this.lg.pasword_user.addKeyListener(this);
        this.lg.btn_ingresar.addMouseListener(this);
        this.lg.jButton1.addMouseListener(this);
    }

    public ControladorLogin(PanelAdmin pna) {
        this.pna = pna;
    }
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == lg.btn_ingresar){
            Ingresar();
        }
    }
    
    public void Ingresar(){
        if(lg.txt_User.getText().isEmpty() || lg.pasword_user.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Campos vacios", "Campos Vacios",JOptionPane.ERROR_MESSAGE);
            }else{
                if(Verificar() == true){
                        PanelAdmin ad = new PanelAdmin();
                        ad.setVisible(true);
                        ad.txt_Usuario.setText(nombreAux);
                        ad.txt_RolUsuario.setText(ROL);
                        if(ad.VerificarRol(ROL) == false){
                            ad.jTabbedPane1.setEnabledAt(3, false);
                            ad.jTabbedPane1.setEnabledAt(5, false);
                            ad.jTabbedPane1.setEnabledAt(7, false);
                            ad.jTabbedPane1.setEnabledAt(9, false);
                        }
                        lg.setVisible(false);
                    
                }else{
                    JOptionPane.showMessageDialog(null, "Usuario o Contraseña Incorrectos\n"
                                                                 + "Intente de nuevo");
                    lg.txt_User.setText("");
                    lg.pasword_user.setText("");
                }
                
            }
    }
    
    public boolean Verificar(){
        String sql = "SELECT * FROM Usuarios WHERE  Usuario = ? AND Contraseña = ?";
        String NomUsuario = lg.txt_User.getText();
        String Contraseña = lg.pasword_user.getText().toString();
        String ContraAUX;
        PreparedStatement ps;
        ResultSet rs;
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, NomUsuario);
            ps.setString(2, Contraseña);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombreAux = (rs.getString("Usuario"));
                ContraAUX = (rs.getString("Contraseña"));
                ROL = (rs.getString("Rol"));
                if(nombreAux.equals(NomUsuario) && ContraAUX.equals(Contraseña)){
                    return true;
                }else{
                    return false;
                }
            }
        }catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        return false;
       }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_ENTER){
               Ingresar();
      }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
 
    }

    @Override
    public void mousePressed(MouseEvent e) {
   
    }

    @Override
    public void mouseReleased(MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() == lg.btn_ingresar){
            lg.btn_ingresar.setBackground(Color.red);
        }else  if(e.getSource() == lg.jButton1){
            lg.jButton1.setBackground(Color.red);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
    if(e.getSource() == lg.btn_ingresar){
            lg.btn_ingresar.setBackground(Color.white);
        }else  if(e.getSource() == lg.jButton1){
            lg.jButton1.setBackground(Color.white);
        }
    }
    
    
    
}
