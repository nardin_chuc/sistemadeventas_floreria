package Controlador;

import Main.Conexion;
import Vistas.PanelAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorProveedor implements ActionListener, MouseListener, KeyListener {

    private PanelAdmin pn;

    public ControladorProveedor(PanelAdmin pn) {
        this.pn = pn;
        limpiar();
        this.pn.btnNuevoProv.addActionListener(this);
        this.pn.btnModificarProv.addActionListener(this);
        this.pn.btnRegistrarProv.addActionListener(this);
        this.pn.TableProveedor.addMouseListener(this);
        this.pn.txtBuscarProv.addKeyListener(this);
        this.pn.JLabelProveedor.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pn.btnNuevoProv) {
            limpiar();
        } else if (e.getSource() == pn.btnModificarProv) {
            if (pn.txtIdProv.getText().isEmpty() || pn.txtNombreProv.getText().isEmpty() || pn.txtTelefonoProv.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                try {
                    String Nombre = pn.txtNombreProv.getText();
                    int id = Integer.parseInt(pn.txtIdProv.getText());
                    int Telefono = Integer.parseInt(pn.txtTelefonoProv.getText());
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection cn = Conexion.establecerConnection();
                    String consulta = "UPDATE Proveedor SET Nombre = ?, Telefono = ? WHERE id = ?";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Nombre);
                    ps.setInt(2, Telefono);
                    ps.setInt(3, id);
                    ps.executeUpdate();
                    limpiar();
                    JOptionPane.showMessageDialog(null, "Se Modifico el Proveedor correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Modifico el Proveedor correctamente");
                }
            }

        } else if (e.getSource() == pn.btnRegistrarProv) {
            if (pn.txtNombreProv.getText().isEmpty() || pn.txtTelefonoProv.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                if (Verificar() == true) {
                    JOptionPane.showMessageDialog(null, "Ya se encuentra registrado en la base de datos");
                    limpiar();
                    pn.txtNombreProv.isFocusable();
                } else {
                    try {
                        String Nombre = pn.txtNombreProv.getText();
                        int Telefono = Integer.parseInt(pn.txtTelefonoProv.getText());
                        PreparedStatement ps;
                        ResultSet rs;
                        Connection cn = Conexion.establecerConnection();
                        String consulta = "INSERT INTO Proveedor (Nombre, Telefono) VALUES (?, ?)";
                        ps = cn.prepareStatement(consulta);
                        ps.setString(1, Nombre);
                        ps.setInt(2, Telefono);
                        ps.executeUpdate();
                        limpiar();
                        JOptionPane.showMessageDialog(null, "Se Ingreso el Proveedor correctamente");
                    } catch (Exception er) {
                        JOptionPane.showMessageDialog(null, "N0 se Ingreso el Proveedor correctamente");
                    }
                }
            }

        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pn.TableProveedor) {
            try {
                int fila = pn.TableProveedor.getSelectedRow();
                String id = pn.TableProveedor.getValueAt(fila, 0).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre, Telefono FROM Proveedor WHERE id = ?");
                ps.setString(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pn.txtIdProv.setText(rs.getString("id"));
                    pn.txtNombreProv.setText(rs.getString("Nombre"));
                    pn.txtTelefonoProv.setText("" + rs.getInt("Telefono"));
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        } else if (e.getSource() == pn.JLabelProveedor) {
            pn.jTabbedPane1.setSelectedIndex(2);
            limpiar();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void cargarTabla(String Nombre) {
        DefaultTableModel modeloTabla = (DefaultTableModel) pn.TableProveedor.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 200};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pn.TableProveedor.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM Proveedor ORDER BY id ASC";
            String consulta = "SELECT *FROM Proveedor WHERE Nombre LIKE '%" + Nombre + "%'";
            if ("".equalsIgnoreCase(Nombre)) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }

        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    public void limpiar() {
        pn.txtIdProv.setText("");
        pn.txtNombreProv.setText("");;
        pn.txtTelefonoProv.setText("");
        pn.txtBuscarProv.setText("");
        cargarTabla(pn.txtBuscarProv.getText());
    }

    public boolean Verificar() {
        String sql = "SELECT * FROM Proveedor WHERE  Nombre = ? ";
        String NomProv = pn.txtNombreProv.getText();
        String nombreAux;
        PreparedStatement ps;
        ResultSet rs;
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, NomProv);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombreAux = (rs.getString("Nombre"));
                if (nombreAux.equals(NomProv)) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }

        return false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        cargarTabla(pn.txtBuscarProv.getText());
    }

}
