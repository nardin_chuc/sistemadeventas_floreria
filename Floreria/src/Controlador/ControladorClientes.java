
package Controlador;

import Main.Conexion;
import Vistas.PanelAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorClientes implements ActionListener, MouseListener, KeyListener{
    
    PanelAdmin pn ;

    public ControladorClientes(PanelAdmin pn) {
        this.pn = pn;
        limpiar();
        this.pn.btnNuevoCli.addActionListener(this);
        this.pn.btnRegitrarCli.addActionListener(this);
        this.pn.btnModificarCli.addActionListener(this);
        this.pn.TableClientes.addMouseListener(this);
        this.pn.txtBuscarCli.addKeyListener(this);
        this.pn.JLabelClientes.addMouseListener(this);
    }
    
    
    
    

       @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pn.btnNuevoCli) {
           limpiar();

        } else if (e.getSource() ==  pn.btnModificarCli) {
            if (pn.txtIdCliente.getText().isEmpty() || pn.txtNombreCli.getText().isEmpty() || pn.txtTelefonoCli.getText().isEmpty() || pn.txtDireccionCli.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            }else{
                try {
                    String Nombre = pn.txtNombreCli.getText(); int id = Integer.parseInt(pn.txtIdCliente.getText()); 
                    int Telefono = Integer.parseInt(pn.txtTelefonoCli.getText());
                    String Direccion = pn.txtDireccionCli.getText();
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection cn = Conexion.establecerConnection();
                   String consulta = "UPDATE Clientes SET Nombre = ?, Telefono = ?, Direccion = ? WHERE id = ?";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Nombre);
                    ps.setInt(2, Telefono);
                    ps.setString(3, Direccion);
                    ps.setInt(4, id);
                    ps.executeUpdate();
                    limpiar();
                    JOptionPane.showMessageDialog(null, "Se Modifico el Proveedor correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Modifico el Proveedor correctamente");
                }
            }

        } else if (e.getSource() == pn.btnRegitrarCli) {
           if ( pn.txtNombreCli.getText().isEmpty() || pn.txtTelefonoCli.getText().isEmpty() || pn.txtDireccionCli.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            }else{
                try {
                    String Nombre = pn.txtNombreCli.getText(); 
                    int Telefono = Integer.parseInt(pn.txtTelefonoCli.getText());
                    String Direccion = pn.txtDireccionCli.getText();
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection cn = Conexion.establecerConnection();
                    String consulta = "INSERT INTO Clientes (Nombre, Telefono, Direccion) VALUES (?, ?, ?)";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Nombre);
                    ps.setInt(2, Telefono);
                    ps.setString(3, Direccion);
                    ps.executeUpdate();
                    limpiar();
                    JOptionPane.showMessageDialog(null, "Se Ingreso el Cliente correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Ingreso el Cliente correctamente");
                }
            }
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pn.TableClientes) {
            try {
                int fila = pn.TableClientes.getSelectedRow();
                String id = pn.TableClientes.getValueAt(fila, 0).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre, Telefono, Direccion FROM Clientes WHERE id = ?");
                ps.setString(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pn.txtIdCliente.setText(""+rs.getInt("id"));
                    pn.txtNombreCli.setText(rs.getString("Nombre"));
                    pn.txtTelefonoCli.setText("" + rs.getInt("Telefono"));
                    pn.txtDireccionCli.setText(rs.getString("Direccion"));
                    
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }else if (e.getSource() == pn.JLabelClientes) {
            pn.jTabbedPane1.setSelectedIndex(1);
            
            limpiar();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    
    public void cargarTabla(String Nombre) {
        DefaultTableModel modeloTabla = (DefaultTableModel) pn.TableClientes.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 200};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pn.TableClientes.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM Clientes ORDER BY id ASC";
            String consulta = "SELECT *FROM Clientes WHERE Nombre LIKE '%"+Nombre+"%'";
            if ("".equalsIgnoreCase(Nombre)) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            }
            
            
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    @Override
    public void keyTyped(KeyEvent e) {
   
    }

    @Override
    public void keyPressed(KeyEvent e) {
   
    }

    @Override
    public void keyReleased(KeyEvent e) {
        cargarTabla(pn.txtBuscarCli.getText());
    }
    
    public void limpiar(){
         pn.txtIdCliente.setText("");
            pn.txtNombreCli.setText("");;
            pn.txtTelefonoCli.setText("");
            pn.txtBuscarCli.setText("");
            pn.txtDireccionCli.setText("");
            cargarTabla(pn.txtBuscarCli.getText());
    }
}
