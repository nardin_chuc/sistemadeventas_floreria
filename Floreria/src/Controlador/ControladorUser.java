/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Main.Conexion;
import Vistas.PanelAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorUser implements ActionListener, MouseListener, KeyListener {

    PanelAdmin pn;
    DefaultTableModel modelo = new DefaultTableModel();

    public ControladorUser(PanelAdmin pn) {
        this.pn = pn;
        limpiar();
        this.pn.btnNuevoUser.addActionListener(this);
        this.pn.btnModificarUser.addActionListener(this);
        this.pn.btnRegitrarUser.addActionListener(this);
        this.pn.TableUsers.addMouseListener(this);
        this.pn.txtBuscarUsers.addKeyListener(this);
        this.pn.JLabelUsers.addMouseListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pn.btnNuevoUser) {
            limpiar();

        } else if (e.getSource() == pn.btnModificarUser) {
            if (pn.txtIdUser.getText().isEmpty() || pn.txtUsuarioUser.getText().isEmpty() || pn.txtNombreUser.getText().isEmpty() || pn.txt_TelefonoUser.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                try {
                    String Nombre = pn.txtNombreUser.getText();
                    int id = Integer.parseInt(pn.txtIdUser.getText());
                    int Telefono = Integer.parseInt(pn.txt_TelefonoUser.getText());
                    String Usuario = pn.txtUsuarioUser.getText();
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection cn = Conexion.establecerConnection();
                    String consulta = "UPDATE Usuarios SET Usuario = ?, Nombre = ?, Telefono = ? WHERE id = ?";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Usuario);
                    ps.setString(2, Nombre);
                    ps.setInt(3, Telefono);
                    ps.setInt(4, id);
                    ps.executeUpdate();
                    limpiar();
                    JOptionPane.showMessageDialog(null, "Se Modifico el Usuario correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Modifico el Usuario correctamente");
                }
            }

        } else if (e.getSource() == pn.btnRegitrarUser) {
            if (pn.txtNombreUser.getText().isEmpty() || pn.txtUsuarioUser.getText().isEmpty() || pn.txt_TelefonoUser.getText().isEmpty() || pn.txtClaveUser.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            } else {
                if (Verificar() == true) {
                    JOptionPane.showMessageDialog(null, "Ya existe el Nombre de Usuario enla base de Datos");
                } else {
                    try {
                        String Usuario = pn.txtUsuarioUser.getText();
                        String Contraseña = (String.valueOf(pn.txtClaveUser.getPassword()));
                        String Nombre = pn.txtNombreUser.getText();
                        int Telefono = Integer.parseInt(pn.txt_TelefonoUser.getText());
                        String Rol = (pn.cbxRolUser.getSelectedItem().toString());
                        PreparedStatement ps;
                        ResultSet rs;
                        Connection cn = Conexion.establecerConnection();
                        String consulta = "INSERT INTO Usuarios (Usuario, Nombre,Contraseña, Telefono, Rol) VALUES (?, ?, ?,?,?)";
                        ps = cn.prepareStatement(consulta);
                        ps.setString(1, Usuario);
                        ps.setString(2, Nombre);
                        ps.setString(3, Contraseña);
                        ps.setInt(4, Telefono);
                        ps.setString(5, Rol);
                        ps.executeUpdate();
                        limpiar();
                        JOptionPane.showMessageDialog(null, "Se Ingreso el Usuario correctamente");
                    } catch (Exception er) {
                        JOptionPane.showMessageDialog(null, "N0 se Ingreso el Usuario correctamente");
                    }
                }
            }
        }

    }

    public boolean Verificar() {
        String sql = "SELECT * FROM Usuarios WHERE  Usuario = ? ";
        String NomUsuario = pn.txtUsuarioUser.getText();
        String nombreAux;
        PreparedStatement ps;
        ResultSet rs;
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, NomUsuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombreAux = (rs.getString("Usuario"));
                if (nombreAux.equals(NomUsuario)) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception er) {
            System.err.println("Error en tabla: " + er.toString());
        }
        return false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pn.TableUsers) {
            if (pn.VerificarRol(pn.txt_RolUsuario.getText())) {
                try {
                    int fila = pn.TableUsers.getSelectedRow();
                    int id = Integer.parseInt(pn.TableUsers.getValueAt(fila, 0).toString());
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection con = Conexion.establecerConnection();
                    ps = con.prepareStatement("SELECT id, Usuario,  Nombre, Contraseña, Telefono, Rol FROM Usuarios WHERE id = ?");
                    ps.setInt(1, id);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        pn.txtIdUser.setText("" + rs.getInt("id"));
                        pn.txtUsuarioUser.setText(rs.getString("Usuario"));
                        pn.txtNombreUser.setText(rs.getString("Nombre"));
                        pn.txtClaveUser.setText(rs.getString("Contraseña"));
                        pn.txt_TelefonoUser.setText("" + rs.getInt("Telefono"));
                        pn.cbxRolUser.setSelectedItem(rs.getString("Rol"));

                    }
                } catch (Exception er) {
                    System.err.println("Error en tabla: " + er.toString());
                }
            }else{
                try {
                    int fila = pn.TableUsers.getSelectedRow();
                    int id = Integer.parseInt(pn.TableUsers.getValueAt(fila, 0).toString());
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection con = Conexion.establecerConnection();
                    ps = con.prepareStatement("SELECT id, Usuario,  Nombre, Telefono, Rol FROM Usuarios WHERE id = ?");
                    ps.setInt(1, id);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        pn.txtIdUser.setText("" + rs.getInt("id"));
                        pn.txtUsuarioUser.setText(rs.getString("Usuario"));
                        pn.txtNombreUser.setText(rs.getString("Nombre"));
                        pn.txt_TelefonoUser.setText("" + rs.getInt("Telefono"));
                        pn.cbxRolUser.setSelectedItem(rs.getString("Rol"));
                    }
                } catch (Exception er) {
                    System.err.println("Error en tabla: " + er.toString());
                }
            }

        } else if (e.getSource() == pn.JLabelUsers) {
            if (pn.VerificarRol(pn.txt_RolUsuario.getText()) == false) {
                pn.JLabelUsers.setEnabled(false);
            } else {
                pn.jTabbedPane1.setSelectedIndex(3);
                limpiar();
            }

        }
    }

    public void limpiarTable() {
        for (int i = 0; i < modelo.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void cargarTabla(String Nombre) {
        DefaultTableModel modeloTabla = (DefaultTableModel) pn.TableUsers.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150,150, 150, 200};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pn.TableUsers.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Usuario, Nombre,Contraseña, Telefono, Rol, Estado FROM Usuarios ORDER BY id ASC";
            String consulta = "SELECT id, Usuario, Nombre, Contraseña, Telefono, Rol, Estado FROM Usuarios WHERE Nombre LIKE '%" + Nombre + "%'";
            if ("".equalsIgnoreCase(Nombre)) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }

        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    public void limpiar() {
        pn.txtIdUser.setText("");
        pn.txtUsuarioUser.setText("");;
        pn.txtNombreUser.setText("");
        pn.txtBuscarUsers.setText("");
        pn.txt_TelefonoUser.setText("");
        cargarTabla(pn.txtBuscarUsers.getText());
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        cargarTabla(pn.txtBuscarUsers.getText());
    }
}
