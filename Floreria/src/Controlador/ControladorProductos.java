/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Main.Conexion;
import Vistas.PanelAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorProductos implements ActionListener, MouseListener, KeyListener{
    
    PanelAdmin pn ;
DefaultTableModel modelo = new DefaultTableModel();

    public ControladorProductos(PanelAdmin pn) {
        this.pn = pn;
        limpiar();
        LLenarCBX();
        this.pn.btnRegistrarPro.addActionListener(this);
        this.pn.btnModificarPro.addActionListener(this);
        this.pn.btnNuevoPro.addActionListener(this);
        this.pn.JlabelProductos.addMouseListener(this);
        this.pn.TableProductos.addMouseListener(this);
        this.pn.txtBuscarProducto.addKeyListener(this);
    }


    
    
       @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pn.btnNuevoPro) {
           limpiar();

        } else if (e.getSource() ==  pn.btnModificarPro) {
            if (pn.txtIdProducto.getText().isEmpty() || pn.txtNombreProd.getText().isEmpty() || pn.txtPrecioCompraPro.getText().isEmpty() || pn.txtPrecioVentaPro.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            }else{
                try {
                    int id = Integer.parseInt(pn.txtIdProducto.getText());
                    String Producto = pn.txtNombreProd.getText();
                    float PrecioCompra = Float.parseFloat(pn.txtPrecioCompraPro.getText()); 
                    float PrecioVenta = Float.parseFloat(pn.txtPrecioVentaPro.getText()); 
                    String Proveedor = (pn.cbxProveedorPro.getSelectedItem().toString());
                    int id_Proveedor = 0;
                    PreparedStatement ps;
                    ResultSet rs;
                    PreparedStatement ps2;
                    ResultSet rs2;
                    Connection cn = Conexion.establecerConnection();
                    String sql = "SELECT id FROM Proveedor WHERE Nombre = ?";
                    ps2 = cn.prepareStatement(sql);
                    ps2.setString(1, Proveedor);
                    rs2 = ps2.executeQuery();
                     while (rs2.next()) {
                     id_Proveedor = rs2.getInt("id");
                    }
                     
                   String consulta = "UPDATE Productos SET Producto = ?, PrecioCompra = ?,PrecioVenta = ?,  id_Proveedor=?, Proveedor=? WHERE id_Producto = ?";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Producto);
                    ps.setFloat(2, PrecioCompra);
                    ps.setFloat(3, PrecioVenta);
                    ps.setInt(4, id_Proveedor);
                    ps.setString(5, Proveedor);
                    ps.setInt(6, id);
                    ps.executeUpdate();
                   limpiar();
                    JOptionPane.showMessageDialog(null, "Se Modifico el Usuario correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Modifico el Usuario correctamente");
                }
            }

        } else if (e.getSource() == pn.btnRegistrarPro) {
           if ( pn.txtNombreProd.getText().isEmpty() || pn.txtPrecioCompraPro.getText().isEmpty() || pn.txtPrecioVentaPro.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos Vacios");
            }else{
              if(Verificar()==true){
                  JOptionPane.showMessageDialog(null, "Ya se encuentra en la base de datos");
            }else{
                 try {
                    PreparedStatement ps;
                    ResultSet rs;
                    PreparedStatement ps2;
                    ResultSet rs2;
                    Connection cn = Conexion.establecerConnection();
                    String Producto = pn.txtNombreProd.getText();
                    float PrecioCompra = Float.parseFloat(pn.txtPrecioCompraPro.getText()); 
                    float PrecioVenta = Float.parseFloat(pn.txtPrecioVentaPro.getText()); 
                    String Proveedor = (pn.cbxProveedorPro.getSelectedItem().toString());
                    int id_Proveedor = 0;
                    String sql = "SELECT id FROM Proveedor WHERE Nombre = ?";
                    ps2 = cn.prepareStatement(sql);
                    ps2.setString(1, Proveedor);
                    rs2 = ps2.executeQuery();
                     while (rs2.next()) {
                     id_Proveedor = rs2.getInt("id");
                    }
//                    INSERT INTO Productos (Producto, PrecioCompra, PrecioVenta, id_Proveedor, Proveedor) Values ('rosas', 5.2,6.3,2,'Coca Cola')
                    String consulta = "INSERT INTO Productos (Producto, PrecioCompra,PrecioVenta, id_Proveedor, Proveedor) VALUES (?, ?, ?,?,?)";
                    ps = cn.prepareStatement(consulta);
                    ps.setString(1, Producto);
                    ps.setFloat(2, PrecioCompra);
                    ps.setFloat(3, PrecioVenta);
                    ps.setInt(4, id_Proveedor);
                    ps.setString(5, Proveedor);
                    ps.executeUpdate();
                    limpiar();
                    JOptionPane.showMessageDialog(null, "Se Ingreso el Producto correctamente");
                } catch (Exception er) {
                    JOptionPane.showMessageDialog(null, "N0 se Ingreso el Producto correctamente");
                }
              }             
            }
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pn.TableProductos) {
            try {
                int fila = pn.TableProductos.getSelectedRow();
                int id = Integer.parseInt(pn.TableProductos.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id_Producto, Producto, PrecioCompra,  PrecioVenta, Proveedor FROM Productos WHERE id_Producto = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pn.txtIdProducto.setText(""+rs.getInt("id_Producto"));
                    pn.txtNombreProd.setText(rs.getString("Producto"));
                    pn.txtPrecioCompraPro.setText(""+rs.getDouble("PrecioCompra"));
                    pn.txtPrecioVentaPro.setText(""+rs.getDouble("PrecioVenta"));
                    pn.cbxProveedorPro.setSelectedItem(rs.getString("Proveedor"));
                    
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }else if (e.getSource() == pn.JlabelProductos) {
            pn.jTabbedPane1.setSelectedIndex(0);
            limpiar();
            LLenarCBX();
        }
    }
        public boolean Verificar(){
        String sql = "SELECT * FROM Productos WHERE  Producto = ? ";
        String NomProducto = pn.txtNombreProd.getText();
        String nombreAux;
        PreparedStatement ps;
        ResultSet rs;
        try {
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, NomProducto);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombreAux = (rs.getString("Producto"));
                if(nombreAux.equals(NomProducto)){
                    return true;
                }else{
                    return false;
                }
            }
        }catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        return false;
       }
    public void limpiarTable() {
        for (int i = 0; i < modelo.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
    
    public void LLenarCBX(){
        try {
                pn.cbxProveedorPro.removeAllItems();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT Nombre FROM Proveedor");
                rs = ps.executeQuery();
                int i = 0;
                while (rs.next()) {
                    String Proveedor = rs.getString("Nombre");
                    pn.cbxProveedorPro.addItem(Proveedor);
                    i++;
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        
    }

    
    public void cargarTabla(String Nombre) {
        DefaultTableModel modeloTabla = (DefaultTableModel) pn.TableProductos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 200};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pn.TableProductos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_Producto, Producto, PrecioVenta, Cantidad, Estado FROM Productos ORDER BY id_Producto ASC";
            String consulta = "SELECT id_Producto, Producto, PrecioVenta, Cantidad, Estado FROM Productos  WHERE Nombre LIKE '%"+Nombre+"%'";
            if ("".equalsIgnoreCase(Nombre) ) {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
            }
            
            
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    public void limpiar(){
         pn.txtIdProducto.setText("");
            pn.txtNombreProd.setText("");
            pn.txtPrecioCompraPro.setText("");
            pn.txtPrecioVentaPro.setText("");
            cargarTabla(pn.txtBuscarProducto.getText());
    }
    @Override
    public void keyTyped(KeyEvent e) {
   
    }

    @Override
    public void keyPressed(KeyEvent e) {
   
    }

    @Override
    public void keyReleased(KeyEvent e) {
        cargarTabla(pn.txtBuscarUsers.getText());
    }
}
